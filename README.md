# Bluetooth Serial Android App

## Disclaimer
This app is a forked version of [xamarin/monodroid-samples](https://github.com/xamarin/monodroid-samples/tree/master/BluetoothChat).

## Activities

This app consists of 3 activities:

1. MainActivity
2. BluetoothService
3. DeviceListActivity

## Key changes from original

### Layout

- Change from ListView to LinearView. 
- Add TextView and Buttons for On and Off.

### Code

- In `BluetoothService.cs`, `MY_UUID` is changed to `00001101-0000-1000-8000-00805F9B34FB`. This is a general UUID for bluetooth serial ([ref](https://developer.android.com/reference/android/bluetooth/BluetoothDevice.html#createInsecureRfcommSocketToServiceRecord(java.util.UUID))).
- Instead of sending texts like in example, we send only one string which is `0` or `1` indicating LED status. This needs corresponding arduino sketch (`.ino`) to work.
- Example arduino sketch is included in this repo at `arduino/btSerialSwitch.ino`.

## Suggestions

- Change `DeviceListActivity` from hiding menu to toggle/switch button next to connection status. This will reduce a number of click user needed to perform.
- Change sending object from `string` to `byte`.